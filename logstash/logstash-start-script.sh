#!/bin/bash

export REPOSITORY_LOGSTASH="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/logstash"
export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/monit"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

wget $REPOSITORY_LOGSTASH/logstash-template.sh --no-cache
chmod +x logstash-template.sh
./logstash-template.sh -d evoila -e openstack
