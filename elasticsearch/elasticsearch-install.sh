#!/bin/bash

groupadd -r elasticsearch
useradd -r -g elasticsearch elasticsearch

mkdir -p $ES_DATA
chown -R elasticsearch:elasticsearch $ES_DATA

#accepts the Oracle JDK8 license automatically
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections

#installs java8
apt-get update && apt-get install -y software-properties-common
yes \n | add-apt-repository ppa:webupd8team/java
apt-get update

apt-get install -y oracle-java8-installer
# automatically set up the Java 8 environment variables
apt-get install oracle-java8-set-default

apt-key adv --keyserver ha.pool.sks-keyservers.net --recv-keys 46095ACC8548582C1A2699A9D27D666CD88E42B4
wget -qO - https://packages.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -

echo "deb http://packages.elastic.co/elasticsearch/2.x/debian stable main" | sudo tee -a /etc/apt/sources.list.d/elasticsearch-2.x.list

set -x \
&& apt-get update \
&& apt-get install -y --no-install-recommends elasticsearch=$ELASTICSEARCH_VERSION \
&& rm -rf /var/lib/apt/lists/*

set -ex \
&& for path in \
/usr/share/elasticsearch/data \
/usr/share/elasticsearch/logs \
/usr/share/elasticsearch/config \
/usr/share/elasticsearch/config/scripts \
	; do \
		mkdir -p "$path"; \
		chown -R elasticsearch:elasticsearch "$path"; \
	done

cp config/logging.yml /usr/share/elasticsearch/config/

mkdir -p /usr/share/elasticsearch/data



# for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
  $OPENSTACK_STOP
fi

# for docker as environment
if [ "$ENVIRONMENT" = 'docker' ]; then
  $DOCKER_STOP
fi
