#!/bin/bash
echo
echo "### Starting configuration for Elasticsearch Cluster... ###"
echo


sudo vi /etc/elasticsearch/elasticsearch.yml

network.host: [_tun0_, _local_]

cluster.name: production

node.name: ${HOSTNAME}
