#!/bin/bash

export REPOSITORY_ELASTICSEARCH_CLUSTER="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/elasticsearch/cluster"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

wget $REPOSITORY_ELASTICSEARCH_CLUSTER/elasticsearch-cluster-template.sh --no-cache
chmod +x elasticsearch-cluster-template.sh
./elasticsearch-cluster-template.sh -p evoila -e openstack
