#!/bin/bash

export REPOSITORY_ELASTICSEARCH="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/elasticsearch"
export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/monit"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

wget $REPOSITORY_ELASTICSEARCH/elasticsearch-template.sh --no-cache
chmod +x elasticsearch-template.sh
./elasticsearch-template.sh -p evoila -e openstack
