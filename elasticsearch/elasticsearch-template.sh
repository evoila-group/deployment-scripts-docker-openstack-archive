#!/bin/bash

export ELASTICSEARCH_MAJOR=2.3
export ELASTICSEARCH_VERSION=2.3.0
export PATH=/usr/share/elasticsearch/bin:$PATH
export ES_DATA=/data/elasticsearch

#path used to check if service is installed
export CHECK_PATH=$ES_DATA/

# start/stop commands for different environments
export OPENSTACK_START="service elasticsearch start"
export OPENSTACK_STOP="service elasticsearch stop"
export DOCKER_START="sudo -u elasticsearch /usr/share/elasticsearch/bin/elasticsearch --path.conf=/etc/elasticsearch/"
export DOCKER_STOP=""

#configuration for logging
export LOG_HOST="172.24.102.12"
export LOG_PORT="5002"
export LOG_SENDING_PROTOCOL="tcp"

#parameters for monit password and environment used for elasticsearch
usage() { echo "Usage: $0 [-e <string>] [-p <string>]" 1>&2; exit 1; }

while getopts ":e:p:" o; do
    case "${o}" in
        p)
            MONIT_PASSWORD=${OPTARG}
            ;;
        e)
            ENVIRONMENT=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${MONIT_PASSWORD}" ] || [ -z "${ENVIRONMENT}" ]; then
    usage
fi

# export for following scripts
export ENVIRONMENT=${ENVIRONMENT}

echo "environment = ${ENVIRONMENT}"
echo "repository_elasticsearch = ${REPOSITORY_ELASTICSEARCH}"
echo "check_path = ${CHECK_PATH}"


# checks if service is installed
if [ -a $CHECK_PATH* ]; then
    # executes script for startup of elasticsearch
    chmod +x elasticsearch-run.sh
    ./elasticsearch-run.sh elasticsearch
else
    if [ "$ENVIRONMENT" = 'openstack' ]; then
        # loads and executes script for logging to a (central) logging instance
        wget $REPOSITORY_ELASTICSEARCH/elasticsearch-logging.sh
        chmod +x elasticsearch-logging.sh
        ./elasticsearch-logging.sh
    fi
    # loads and executes script for automatic installation of elasticsearch
    wget $REPOSITORY_ELASTICSEARCH/elasticsearch-install.sh
    chmod +x elasticsearch-install.sh
    ./elasticsearch-install.sh

    # loads includes for the monit controlfile for this database
    mkdir -p /etc/monit.d/
    wget $REPOSITORY_ELASTICSEARCH/elasticsearch-monitrc -P /etc/monit.d/

    # loads and executes script for configuration of elasticsearch
    wget $REPOSITORY_ELASTICSEARCH/elasticsearch-configuration.sh
    chmod +x elasticsearch-configuration.sh
    ./elasticsearch-configuration.sh

    # loads and executes script for startup of elasticsearch
    wget $REPOSITORY_ELASTICSEARCH/elasticsearch-run.sh
    chmod +x elasticsearch-run.sh
    ./elasticsearch-run.sh elasticsearch
fi

# installs monit for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
  export REPOSITORY_MONIT="${REPOSITORY_MONIT}"
  wget $REPOSITORY_MONIT/monit-template.sh
  chmod +x monit-template.sh
  ./monit-template.sh -u monit -p ${MONIT_PASSWORD}
fi
