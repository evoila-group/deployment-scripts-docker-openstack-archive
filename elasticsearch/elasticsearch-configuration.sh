#!/bin/bash
echo
echo "### Starting configuration of Elasticsearch ... ###"
echo

set -e

# overwrites elasticsearch config file
sed -i "s|^#path.data:.*$|path.data: $ES_DATA|" "/etc/elasticsearch/elasticsearch.yml"
sed -i "s|^# network.host:.*$|network.host: 0.0.0.0|" "/etc/elasticsearch/elasticsearch.yml"
sed -i "s|^# http.port:.*$|http.port: 9200|" "/etc/elasticsearch/elasticsearch.yml"

#rm -R /usr/share/elasticsearch/data

# Drop root privileges if we are running elasticsearch
#if [ "$1" = 'elasticsearch' ]; then
	# Change the ownership of /usr/share/elasticsearch/data to elasticsearch
#	chown -R elasticsearch:elasticsearch $ES_DATA
#fi
