#!/bin/bash
export MYSQL_MAJOR=5.7
export MYSQL_VERSION=5.7.14-1ubuntu14.04
export DATADIR=/data/mysql

#path used to check if service is installed
export CHECK_PATH=$DATADIR

# start/stop commands for different environments
export OPENSTACK_START="service mysql start"
export OPENSTACK_STOP="service mysql stop"
export DOCKER_START="mysqld"
export DOCKER_STOP="mysqld stop"

#configuration for logging
export LOG_HOST="172.24.102.12"
export LOG_PORT="5002"
export LOG_SENDING_PROTOCOL="tcp"


#parameters for dbname, user and password and environment used in mysql-configuration.sh and mysql-run.sh
usage() { echo "Usage: $0 [-d <string>] [-p <string>] [-e <string>]" 1>&2; exit 1; }

while getopts ":d:p:e:" o; do
    case "${o}" in
        d)
            MYSQL_DATABASE=${OPTARG}
            ;;
        p)
            MYSQL_ROOT_PASSWORD=${OPTARG}
            ;;
        e)
            ENVIRONMENT=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

shift $((OPTIND-1))

if [ -z "${MYSQL_DATABASE}" ] || [ -z "${MYSQL_ROOT_PASSWORD}" ] || [ -z "${ENVIRONMENT}" ]; then
    usage
fi


# export for following scripts
export MYSQL_DATABASE=${MYSQL_DATABASE}
export MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
export ENVIRONMENT=${ENVIRONMENT}

echo "mysql_database = ${MYSQL_DATABASE}"
echo "datadir = ${DATADIR}"
echo "environment = ${ENVIRONMENT}"
echo "repository_mysql = ${REPOSITORY_MYSQL}"
echo "check_path = ${CHECK_PATH}"


# checks if service is installed
if [ -a $CHECK_PATH* ]; then
    # executes script for startup of mysql
    chmod +x mysql-run.sh
    ./mysql-run.sh mysqld
else
    if [ "$ENVIRONMENT" = 'openstack' ]; then
      # loads and executes script for logging to a (central) logging instance
      wget $REPOSITORY_MYSQL/mysql-logging.sh
      chmod +x mysql-logging.sh
      ./mysql-logging.sh
    fi
    # loads and executes script for automatic installation of mysql
    wget $REPOSITORY_MYSQL/mysql-install.sh
    chmod +x mysql-install.sh
    ./mysql-install.sh

    # loads includes for the monit controlfile for this database
    mkdir -p /etc/monit.d/
    wget $REPOSITORY_MYSQL/mysql-monitrc -P /etc/monit.d/

    # loads and executes script for configuration of mysql
    wget $REPOSITORY_MYSQL/mysql-configuration.sh
    chmod +x mysql-configuration.sh
    ./mysql-configuration.sh

    # loads and executes script for startup of mysql
    wget $REPOSITORY_MYSQL/mysql-run.sh
    chmod +x mysql-run.sh
    ./mysql-run.sh mysqld
fi

# installs monit for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
  export REPOSITORY_MONIT="${REPOSITORY_MONIT}"
  wget $REPOSITORY_MONIT/monit-template.sh
  chmod +x monit-template.sh
  ./monit-template.sh -u monit -p ${MYSQL_ROOT_PASSWORD}
fi
