#!/bin/bash

export REPOSITORY_MYSQL="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/mysql"
export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/monit"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

wget $REPOSITORY_MYSQL/mysql-template.sh --no-cache
chmod +x mysql-template.sh
./mysql-template.sh -d evoila -p evoila -e openstack
