#!/bin/bash

export CHECK_PATH=/data/redis-cluster-check

#parameters for dbname, user and password used in postgres-entrypoint.sh
usage() { echo "Usage: $0 [-p <string>] [-a <string>] [-i <string>] [-n <string>]" 1>&2; exit 1; }

while getopts ":p:a:i:n:" o; do
    case "${o}" in
        p)
            REDIS_PASSWORD=${OPTARG}
            ;;
        a)
            HA_INSTANCE_TYPE=${OPTARG}
            ;;
        i)
            PRIMARY_IP=${OPTARG}
            ;;
        n)
            INSTANCE_ID=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${REDIS_PASSWORD}" ] || [ -z "${HA_INSTANCE_TYPE}" ] || [ -z "${PRIMARY_IP}" ] || [ -z "${INSTANCE_ID}" ]; then
    usage
fi

#exports from parameters for following scripts
export HA_INSTANCE_TYPE=${HA_INSTANCE_TYPE}
export PRIMARY_IP=${PRIMARY_IP}
export REDIS_PASSWORD=${REDIS_PASSWORD}
export INSTANCE_ID=${INSTANCE_ID}

echo "ha_instance_type = ${HA_INSTANCE_TYPE}"
echo "primary_ip = ${PRIMARY_IP}"
echo "redis_password = ${REDIS_PASSWORD}"
echo "instance_id = ${INSTANCE_ID}"


## checks if service is installed
#if [ -a $CHECK_PATH* ]; then
#  wget $REPOSITORY_REDIS_CLUSTER/redis-cluster-run.sh
#  chmod +x redis-cluster-run.sh
#  ./redis-cluster-run.sh

#else
  if [ "$HA_INSTANCE_TYPE" = 'primary' ]; then
    # loads and executes script for configuration of the primary-mysql-instance for HA
    echo "Running configuration for primary node"
    wget $REPOSITORY_REDIS/redis-cluster-primary-conf.sh
    chmod +x redis-cluster-primary-conf.sh
    ./redis-cluster-primary-conf.sh
  fi

  if [ "$HA_INSTANCE_TYPE" = 'secondary' ]; then
    # loads and executes script for configuration of the secondary-mysql-instance for HA
    echo "Running configuration for secondary node"
    wget $REPOSITORY_REDIS/redis-cluster-secondary-conf.sh
    chmod +x redis-cluster-secondary-conf.sh
    ./redis-cluster-secondary-conf.sh
  fi
#fi
