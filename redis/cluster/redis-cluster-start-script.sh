#!/bin/bash

# This script only works with pre-installed and configured redis-server

export REPOSITORY_REDIS="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/redis/cluster"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

# Put in here the IP-Adresses for the Primary-Instance of the Redis-Cluster-Configuration
export PRIMARY_IP="192.168.1.201"

wget $REPOSITORY_REDIS/redis-cluster-template.sh --no-cache
chmod +x redis-cluster-template.sh
./redis-cluster-template.sh -p evoila -i ${PRIMARY_IP} -n 1 -a primary




###################### Secondary Configuration instance 2 ######################

export REPOSITORY_REDIS="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/redis/cluster"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

# Put in here the IP-Adresses for the Primary-Instance of the Redis-Cluster-Configuration
export PRIMARY_IP="192.168.1.201"

wget $REPOSITORY_REDIS/redis-cluster-template.sh --no-cache
chmod +x redis-cluster-template.sh
./redis-cluster-template.sh -p evoila -i ${PRIMARY_IP} -n 2 -a secondary




###################### Secondary Configuration instance 3 ######################

export REPOSITORY_REDIS="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/redis/cluster"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

# Put in here the IP-Adresses for the Primary-Instance of the Redis-Cluster-Configuration
export PRIMARY_IP="192.168.1.201"

wget $REPOSITORY_REDIS/redis-cluster-template.sh --no-cache
chmod +x redis-cluster-template.sh
./redis-cluster-template.sh -p evoila -i ${PRIMARY_IP} -n 3 -a secondary
