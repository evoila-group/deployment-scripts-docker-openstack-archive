#!/bin/bash

export REDIS_VERSION=3.0.7
export VOLUME=/data/redis
export WORKDIR=/data/redis

#path used to check if service is installed
export CHECK_PATH=/data/redis

#configuration for logging
export LOG_HOST="172.24.102.12"
export LOG_PORT="5002"
export LOG_SENDING_PROTOCOL="tcp"

#parameters for vhost, user and password used in redis-entrypoint.sh
usage() { echo "Usage: $0 [-n <string>] [-p <string>] [-e <string>] " 1>&2; exit 1; }

while getopts ":n:p:e:" o; do
    case "${o}" in
        n)
            REDIS_NUMBER_OF_DATABASES=${OPTARG}
            ;;
        p)
            REDIS_PASSWORD=${OPTARG}
            ;;
        e)
            ENVIRONMENT=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${REDIS_NUMBER_OF_DATABASES}" ] || [ -z "${REDIS_PASSWORD}" ] || [ -z "${ENVIRONMENT}" ]; then
    usage
fi

export REDIS_NUMBER_OF_DATABASES=${REDIS_NUMBER_OF_DATABASES}
export REDIS_PASSWORD=${REDIS_PASSWORD}
export ENVIRONMENT=${ENVIRONMENT}

echo "redis_number_of_databases = ${REDIS_NUMBER_OF_DATABASES}"
echo "environment = ${ENVIRONMENT}"
echo "repository_redis = ${REPOSITORY_REDIS}"
echo "check_path = ${CHECK_PATH}"

# checks if service is installed
if [ -a $CHECK_PATH* ]; then
    # executes script for startup of redis
    chmod +x redis-run.sh
    ./redis-run.sh redis-server
else
    if [ "$ENVIRONMENT" = 'openstack' ]; then
      # loads and executes script for logging to a (central) logging instance
      wget $REPOSITORY_REDIS/redis-logging.sh
      chmod +x redis-logging.sh
      ./redis-logging.sh
    fi
    # loads and executes script for automatic installation of redis-server
    wget $REPOSITORY_REDIS/redis-install.sh
    chmod +x redis-install.sh
    ./redis-install.sh

    # loads includes for the monit controlfile for this database
    mkdir -p /etc/monit.d/
    wget $REPOSITORY_REDIS/redis-monitrc -P /etc/monit.d/

    # loads and executes script for configuration of redis
    wget $REPOSITORY_REDIS/redis-configuration.sh
    chmod +x redis-configuration.sh
    ./redis-configuration.sh

    # loads and executes script for startup of redis
    wget $REPOSITORY_REDIS/redis-run.sh
    chmod +x redis-run.sh
    ./redis-run.sh redis-server
fi

# installs monit for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
  export REPOSITORY_MONIT="${REPOSITORY_MONIT}"
  wget $REPOSITORY_MONIT/monit-template.sh
  chmod +x monit-template.sh
  ./monit-template.sh -u monit -p ${REDIS_PASSWORD}
fi
