#!/bin/bash
echo
echo "### Starting configuration of Redis-Server ... ###"
echo
#starts with parameters for number of databases and password set in redis-template.sh
echo "redis_number_of_databases = ${REDIS_NUMBER_OF_DATABASES}"
echo "environment = ${ENVIRONMENT}"

#redis-entrypoint.sh

#creating config-file for redis
mkdir -p /etc/redis/

mkdir -p /var/log/redis/
chown redis:redis /var/log/redis/

echo "# Redis configuration file EVOILA

################################ GENERAL  #####################################

# By default Redis does not run as a daemon. Use 'yes' if you need it.
# Note that Redis will write a pid file in /var/run/redis.pid when daemonized.
# daemonize yes

# When running daemonized, Redis writes a pid file in /var/run/redis.pid by
# default. You can specify a custom pid file location here.
pidfile /var/run/redis/redis.pid

# Set the number of databases. The default database is DB 0, you can select
# a different one on a per-connection basis using SELECT <dbid> where
# dbid is a number between 0 and 'databases'-1
databases ${REDIS_NUMBER_OF_DATABASES}

# Accept connections on the specified port, default is 6379.
# If port 0 is specified Redis will not listen on a TCP socket.
port 6379

################################## SECURITY ###################################

# Require clients to issue AUTH <PASSWORD> before processing any other
# commands.  This might be useful in environments in which you do not trust
# others with access to the host running redis-server.
#
# This should stay commented out for backward compatibility and because most
# people do not need auth (e.g. they run their own servers).
#
# Warning: since Redis is pretty fast an outside user can try up to
# 150k passwords per second against a good box. This means that you should
# use a very strong password otherwise it will be very easy to break.
#
requirepass ${REDIS_PASSWORD}

################################### LOGGING ###################################

# Specify the server verbosity level.
# This can be one of:
# debug (a lot of information, useful for development/testing)
# verbose (many rarely useful info, but not a mess like the debug level)
# notice (moderately verbose, what you want in production probably)
# warning (only very important / critical messages are logged)
loglevel notice

# Specify the log file name. Also the empty string can be used to force
# Redis to log on the standard output. Note that if you use standard
# output for logging but daemonize, logs will be sent to /dev/null
logfile \"/var/log/redis/redis.log\"

# To enable logging to the system logger, just set 'syslog-enabled' to yes,
# and optionally update the other syslog parameters to suit your needs.
# syslog-enabled no

# Specify the syslog identity.
# syslog-ident redis

# Specify the syslog facility. Must be USER or between LOCAL0-LOCAL7.
# syslog-facility local0
" > /etc/redis/redis.conf



# overwrites monitrc
sed -i "s|^  stop program =.*$|  stop program = \"/usr/local/bin/redis-cli -a ${REDIS_PASSWORD} SHUTDOWN SAVE\"|" "/etc/monit.d/redis-monitrc"


# additional config for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
   sed -i "s/^# daemonize yes.*$/  daemonize yes/" "/etc/redis/redis.conf"
fi


#creates directory for redis pid-file
mkdir -p /var/run/redis
chown redis /var/run/redis
