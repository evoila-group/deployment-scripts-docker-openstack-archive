#!/bin/bash

# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
groupadd -r redis && useradd -r -g redis redis

apt-get update && apt-get install -y --no-install-recommends \
ca-certificates \
curl \
&& rm -rf /var/lib/apt/lists/*

export REDIS_DOWNLOAD_URL=http://download.redis.io/releases/redis-3.0.3.tar.gz
export REDIS_DOWNLOAD_SHA1=0e2d7707327986ae652df717059354b358b83358

# for redis-sentinel see: http://redis.io/topics/sentinel
buildDeps='gcc libc6-dev make' \
&& set -x \
&& apt-get update && apt-get install -y $buildDeps --no-install-recommends \
&& rm -rf /var/lib/apt/lists/* \
&& mkdir -p /usr/src/redis \
&& curl -sSL "$REDIS_DOWNLOAD_URL" -o redis.tar.gz \
&& echo "$REDIS_DOWNLOAD_SHA1 *redis.tar.gz" | sha1sum -c - \
&& tar -xzf redis.tar.gz -C /usr/src/redis --strip-components=1 \
&& rm redis.tar.gz \
&& make -C /usr/src/redis \
&& make -C /usr/src/redis install \
&& rm -r /usr/src/redis \
&& apt-get purge -y --auto-remove $buildDeps

mkdir -p /data/redis && chown redis:redis /data/redis
