#!/bin/bash

export REPOSITORY_REDIS="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/redis"
export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/monit"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

wget $REPOSITORY_REDIS/redis-template.sh --no-cache
chmod +x redis-template.sh
./redis-template.sh -n 15 -p evoila -e openstack
