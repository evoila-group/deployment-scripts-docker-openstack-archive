#!/bin/bash
# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
mkdir -p /var/lib/rabbitmq
groupadd -r rabbitmq && useradd -r -d /var/lib/rabbitmq -m -g rabbitmq rabbitmq

mkdir -p /var/run/rabbitmq
mkdir -p $DATA_PATH/db
mkdir -p $DATA_PATH/plugins

chown -R rabbitmq:rabbitmq /var/lib/rabbitmq
chown -R rabbitmq:rabbitmq /var/run/rabbitmq
chown -R rabbitmq:rabbitmq $DATA_PATH

export RABBITMQ_BASE=$DATA_PATH
export RABBITMQ_MNESIA_BASE=$DATA_PATH
export RABBITMQ_MNESIA_DIR=$DATA_PATH/db
export RABBITMQ_PLUGINS_EXPAND_DIR=$DATA_PATH/plugins
export RABBITMQ_PID_FILE=/var/run/rabbitmq/

apt-get update && apt-get install -y --force-yes curl ca-certificates --no-install-recommends && rm -rf /var/lib/apt/lists/*

# grab tini for signal processing and zombie killing
set -x \
&& curl -fSL "https://github.com/krallin/tini/releases/download/v0.5.0/tini" -o /usr/local/bin/tini \
&& chmod +x /usr/local/bin/tini \
&& tini -h

# Add the officially endorsed Erlang debian repository:
# See:
#  - http://www.erlang.org/download.html
#  - https://www.erlang-solutions.com/downloads/download-erlang-otp
apt-key adv --keyserver ha.pool.sks-keyservers.net --recv-keys 434975BD900CCBE4F7EE1B1ED208507CA14F4FCA
echo 'deb http://packages.erlang-solutions.com/ubuntu trusty contrib' > /etc/apt/sources.list.d/erlang.list

# get logs to stdout (thanks @dumbbell for pushing this upstream! :D)
# export RABBITMQ_LOGS=- RABBITMQ_SASL_LOGS=-
# https://github.com/rabbitmq/rabbitmq-server/commit/53af45bf9a162dec849407d114041aad3d84feaf

# http://www.rabbitmq.com/install-debian.html
# "Please note that the word testing in this line refers to the state of our release of RabbitMQ, not any particular Debian distribution."
apt-key adv --keyserver ha.pool.sks-keyservers.net --recv-keys 0A9AF2115F4687BD29803A206B73A36E6026DFCA
  echo 'deb http://www.rabbitmq.com/debian/ kitten main' > /etc/apt/sources.list.d/rabbitmq.list

apt-get update && apt-get install -y --force-yes rabbitmq-server --no-install-recommends && rm -rf /var/lib/apt/lists/*

# /usr/sbin/rabbitmq-server has some irritating behavior, and only exists to "su - rabbitmq /usr/lib/rabbitmq/bin/rabbitmq-server ..."
echo '[{rabbit, [{loopback_users, []}]}].' > /etc/rabbitmq/rabbitmq.config

# add a symlink to the .erlang.cookie in /root so we can "docker exec rabbitmqctl ..."
ln -sf /var/lib/rabbitmq/.erlang.cookie /root/
