#!/bin/bash

export REPOSITORY_RABBITMQ="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/rabbitmq"
export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/monit"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

wget $REPOSITORY_RABBITMQ/rabbitmq-template.sh --no-cache
chmod +x rabbitmq-template.sh
./rabbitmq-template.sh -d evoila -u evoila -p evoila -e openstack
