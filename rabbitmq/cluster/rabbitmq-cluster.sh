#!/bin/bash
echo ""
echo ""
echo "### Starting cluster setup for rabbitmq ###"
echo ""

export PATH=/usr/lib/rabbitmq/bin:$PATH
export HOME=/var/lib/rabbitmq
export DATA_PATH=/data/rabbitmq

export ENVIRONMENT=${ENVIRONMENT}

#path used to check if service is installed
export CHECK_PATH=$DATA_PATH

usage() {
  echo "Error: $1"
  echo "-e: erlang key; -t: host type [primary | secondary]; -m: master host (mandatory for secondary)"
  echo "Usage: $0 [-e <string>] [-t <string>] [-m <string>]"
  1>&2;
  exit 1;
}

while getopts ":e:t:m:" o; do
    case "${o}" in
        e)
            ERLANG_KEY=${OPTARG}
            ;;
        t)
            HOST_TYPE=${OPTARG}
            ;;
        m)
            MASTER=${OPTARG}
            ;;
        *)
            usage "unknown parameter"
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${ERLANG_KEY}" ] || [ -z "${HOST_TYPE}" ]; then
    usage "missing parameter"
fi

if [ "$HOST_TYPE" = 'secondary' ]; then
  if [ -z "${MASTER}" ]; then
      usage "master must be set for secondary hosts"
  fi
fi

echo "Wait for monit to start - 10 sec"
sleep 10
monit stop rabbitmq-server

echo "Wait for monit stop rabbitmq-server - 15 sec"
sleep 15
monit summary

echo "Copy ERLANG_KEY to /var/lib/rabbitmq/.erlang.cookie"
echo ${ERLANG_KEY} > /var/lib/rabbitmq/.erlang.cookie

echo "Restart rabbitmq-server - wait 15 sec"
monit start rabbitmq-server
sleep 15


if [ "$HOST_TYPE" = 'secondary' ]; then
  monit unmonitor rabbitmq-server
  echo "Stop_app for rabbitmq-server - wait 15 sec"
  rabbitmqctl stop_app
  sleep 15
  echo rabbitmqctl join_cluster rabbit@${MASTER}
  rabbitmqctl join_cluster rabbit@${MASTER}
  echo "Wait for start_app rabbitmq-server - 15 sec"
  sleep 15
  monit monitor rabbitmq-server
fi

echo "Cluster Status:"
rabbitmqctl cluster_status
