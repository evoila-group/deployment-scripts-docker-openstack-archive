#!/bin/bash


export PATH=/usr/lib/rabbitmq/bin:$PATH
export HOME=/var/lib/rabbitmq
export DATA_PATH=/data/rabbitmq

export ENVIRONMENT=${ENVIRONMENT}

#path used to check if service is installed
export CHECK_PATH=$DATA_PATH

# start/stop commands for different environments
export OPENSTACK_START="service rabbitmq-server start"
export OPENSTACK_STOP="service rabbitmq-server stop"
export DOCKER_START="/usr/lib/rabbitmq/bin/rabbitmq-server"
export DOCKER_STOP="invoke-rc.d rabbitmq-server stop"

#parameters for vhost, user and password used in rabbitmq-entrypoint.sh
usage() {
  echo "Error: $1"
  echo "-d: host; -u: user; p: password; e: environment"
  echo "all parameters are mandatory - testCM"
  echo "Usage: $0 [-d <string>] [-u <string>] [-p <string>] [-e <string>] "
  1>&2;
  exit 1;
}

while getopts ":d:p:u:e:" o; do
    case "${o}" in
        d)
            RABBITMQ_DEFAULT_VHOST=${OPTARG}
            ;;
        p)
            RABBITMQ_DEFAULT_PASS=${OPTARG}
            ;;
        u)
            RABBITMQ_DEFAULT_USER=${OPTARG}
            ;;
        e)
            ENVIRONMENT=${OPTARG}
            ;;
        *)
            usage "unknown parameter -> ${o}"
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${RABBITMQ_DEFAULT_VHOST}" ] || [ -z "${RABBITMQ_DEFAULT_USER}" ] || [ -z "${RABBITMQ_DEFAULT_PASS}" ] || [ -z "${ENVIRONMENT}" ] ; then
    usage "missing parameter"
fi

#configuration for logging
export RABBITMQ_DEFAULT_VHOST=${RABBITMQ_DEFAULT_VHOST}
export RABBITMQ_DEFAULT_USER=${RABBITMQ_DEFAULT_USER}
export RABBITMQ_DEFAULT_PASS=${RABBITMQ_DEFAULT_PASS}
export ENVIRONMENT=${ENVIRONMENT}

echo "rabbitmq_default_vhost = ${RABBITMQ_DEFAULT_VHOST}"
echo "rabbitmq_default_user = ${RABBITMQ_DEFAULT_USER}"
echo "repository_rabbitmq = ${REPOSITORY_RABBITMQ}"
echo "check_path = ${CHECK_PATH}"


# checks if service is installed
if [ -a $CHECK_PATH* ]; then
    # executes script for startup of rabbitmq-server
    chmod +x rabbitmq-run.sh
    ./rabbitmq-run.sh rabbitmq-server
else
    if [ "$ENVIRONMENT" = 'openstack' ]; then
      # loads and executes script for logging to a (central) logging instance
      wget $REPOSITORY_RABBITMQ/rabbitmq-logging.sh
      chmod +x rabbitmq-logging.sh
      ./rabbitmq-logging.sh
    fi
    # loads and executes script for automatic installation of rabbitmq-server
    wget $REPOSITORY_RABBITMQ/rabbitmq-install.sh
    chmod +x rabbitmq-install.sh
    ./rabbitmq-install.sh

    # loads includes for the monit controlfile for this database
    mkdir -p /etc/monit.d/
    wget $REPOSITORY_RABBITMQ/rabbitmq-monitrc -P /etc/monit.d/

    # loads and executes script for configuration of rabbitmq-server
    wget $REPOSITORY_RABBITMQ/rabbitmq-configuration.sh
    chmod +x rabbitmq-configuration.sh
    ./rabbitmq-configuration.sh

    # loads and executes script for startup of rabbitmq-server
    wget $REPOSITORY_RABBITMQ/rabbitmq-run.sh
    chmod +x rabbitmq-run.sh
    ./rabbitmq-run.sh rabbitmq-server
fi

# installs monit for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
  export REPOSITORY_MONIT="${REPOSITORY_MONIT}"
  wget $REPOSITORY_MONIT/monit-template.sh
  chmod +x monit-template.sh
  ./monit-template.sh -u monit -p ${RABBITMQ_DEFAULT_PASS}
fi
