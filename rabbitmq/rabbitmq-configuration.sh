#!/bin/bash
echo
echo "### Starting configuration of rabbitmq-server ... ###"
echo
#starts with parameters for dbname, user and password set in rabbitmq-template.sh
echo "rabbitmq_default_vhost = ${RABBITMQ_DEFAULT_VHOST}"
echo "rabbitmq_default_user = ${RABBITMQ_DEFAULT_USER}"

echo "RABBITMQ_BASE=$DATA_PATH
RABBITMQ_MNESIA_BASE=$DATA_PATH
RABBITMQ_MNESIA_DIR=$DATA_PATH/db
RABBITMQ_PLUGINS_EXPAND_DIR=$DATA_PATH/plugins
RABBITMQ_PID_FILE=/var/run/rabbitmq/pid" > /etc/rabbitmq/rabbitmq-env.conf

# overwrites rabbitmq-env.conf for docker as environment
if [ "$ENVIRONMENT" = 'docker' ]; then
  sed -i "s|^RABBITMQ_PID_FILE=/var/run/rabbitmq/.*$|RABBITMQ_PID_FILE=/var/run/rabbitmq/pid|" "/etc/rabbitmq/rabbitmq-env.conf"

  /usr/sbin/rabbitmq-server &

  sleep 20

  ls -al /var/lib/rabbitmq/
  chown -R rabbitmq:rabbitmq /var/lib/rabbitmq/.erlang.cookie
fi

rabbitmq-plugins enable rabbitmq_management

#adds new user with admin tag, sets password, sets new vhost and deletes default admin and vhost
rabbitmqctl add_user ${RABBITMQ_DEFAULT_USER} ${RABBITMQ_DEFAULT_PASS}
rabbitmqctl set_user_tags ${RABBITMQ_DEFAULT_USER} administrator
rabbitmqctl add_vhost ${RABBITMQ_DEFAULT_VHOST}
rabbitmqctl set_permissions -p ${RABBITMQ_DEFAULT_VHOST} ${RABBITMQ_DEFAULT_USER} ".*" ".*" ".*"
rabbitmqctl delete_user guest
rabbitmqctl delete_vhost /

# Stop RabbitMQ service
# for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
  $OPENSTACK_STOP
fi
# for docker as environment
if [ "$ENVIRONMENT" = 'docker' ]; then
  $DOCKER_STOP &&
fi
