#!/bin/bash

export REPOSITORY_POSTGRES="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/postgres"
export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/monit"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

wget $REPOSITORY_POSTGRES/postgres-template.sh --no-cache
chmod +x postgres-template.sh
./postgres-template.sh -u evoila -p evoila -d evoila -e openstack
