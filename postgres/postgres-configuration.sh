#!/bin/bash
echo
echo "### Starting configuration of PostgreSQL ... ###"
echo
#starts with parameters for dbname, user and password set in postgres-template.sh

echo "postgres_db = ${POSTGRES_DB}"
echo "postgres_user = ${POSTGRES_USER}"
echo "environment = ${ENVIRONMENT}"

# postgres-entrypoint

set -e

# overwrites default config
sed -i "s|^data_directory =.*$|data_directory = '$PGDATA'                \# use data in another directory|" "/etc/postgresql/$PG_VERSION/main/postgresql.conf"
sed -i "s|^hba_file =.*$|hba_file = '$PGDATA/pg_hba.conf'  			\# host-based authentication file|" "/etc/postgresql/$PG_VERSION/main/postgresql.conf"
sed -i "s|^ident_file =.*$|ident_file = '$PGDATA/pg_ident.conf'      \# ident configuration file|" "/etc/postgresql/$PG_VERSION/main/postgresql.conf"


set_listen_addresses() {
	sedEscapedValue="$(echo "$1" | sed 's/[\/&]/\\&/g')"
	sed -ri "s/^#?(listen_addresses\s*=\s*)\S+/\1'$sedEscapedValue'/" "/etc/postgresql/$PG_VERSION/main/postgresql.conf"
}
set_listen_addresses '*'

mkdir -p "$PGDATA"
chown -R postgres "$PGDATA"

chmod g+s /run/postgresql
chown -R postgres /run/postgresql

sudo -u postgres $PGPATH/initdb -D "$PGDATA"


# check password first so we can output the warning before postgres
# messes it up
if [ "$POSTGRES_PASSWORD" ]; then
	pass="PASSWORD '$POSTGRES_PASSWORD'"
	authMethod=md5

else
	# The - option suppresses leading tabs but *not* spaces. :)
	cat >&2 <<-'EOWARN'
		****************************************************
		WARNING: No password has been set for the database.
		         This will allow anyone with access to the
		         Postgres port to access your database. In
		         Docker's default configuration, this is
		         effectively any other container on the same
		         system.

		         Use "-e POSTGRES_PASSWORD=password" to set
		         it in "docker run".
		****************************************************
	EOWARN

	pass=
	authMethod=trust
fi

{ echo; echo "host all all 0.0.0.0/0 $authMethod"; } >> "$PGDATA/pg_hba.conf"


# internal start of server in order to allow set-up using psql-client
# does not listen on TCP/IP and waits until start finishes
sudo -u postgres $PGPATH/pg_ctl -D "$PGDATA" \
	-o "-c listen_addresses='*'" \
	-w start


: ${POSTGRES_USER:=postgres}
: ${POSTGRES_DB:=$POSTGRES_USER}
export POSTGRES_USER POSTGRES_DB

if [ "$POSTGRES_DB" != 'postgres' ]; then
	sudo -u postgres psql --username postgres <<-EOSQL
		CREATE DATABASE "$POSTGRES_DB" ;
	EOSQL
	echo
fi

if [ "$POSTGRES_USER" = 'postgres' ]; then
	op='ALTER'
else
	op='CREATE'
fi

sudo -u postgres psql --username postgres <<-EOSQL
	$op USER "$POSTGRES_USER" WITH SUPERUSER CREATEROLE $pass ;
EOSQL
echo

sudo -u postgres $PGPATH/pg_ctl -D "$PGDATA" -m fast -w stop

echo
echo 'PostgreSQL init process complete; ready for start up.'
echo
