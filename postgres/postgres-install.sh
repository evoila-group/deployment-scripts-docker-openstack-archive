#!/bin/bash
echo
echo "openstack_start: $OPENSTACK_START"
echo "openstack_stop: $OPENSTACK_STOP"
echo "docker_start: $DOCKER_START"
echo "docker_stop: $DOCKER_STOP"
echo

mkdir -p /home/postgres

groupadd -r postgres
useradd -r -g postgres postgres
apt-get update

chown -R postgres:postgres /home/postgres
chmod 760 -R /home/postgres

# creates datadir
mkdir -p $PGDATA
chown -R postgres:postgres $PGDATA/
chmod 700 -R $PGDATA/

apt-get install -y postgresql postgresql-common postgresql-contrib

# stopps postgresql
  # for openstack as environment
  if [ "$ENVIRONMENT" = 'openstack' ]; then
   $OPENSTACK_STOP
  fi
  # for docker as environment
  if [ "$ENVIRONMENT" = 'docker' ]; then
   $DOCKER_STOP
  fi
