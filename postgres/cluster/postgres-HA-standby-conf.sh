#!/bin/bash
echo
echo "### Configuration of standby-server for High Availability ###"
echo

monit unmonitor postgres
monit summary

echo "Stopping PostgreSQL"
service postgresql stop

# backing up the primary server to the standby server
echo "Cleaning up old cluster directory"
mv $PGDATA ${PGDATA}_old
mkdir -p "$PGDATA"
chown -R postgres "$PGDATA"
chmod 700 -R "$PGDATA"

# edit postgresql.conf
echo "Changing values in /etc/postgresql/$PG_VERSION/main/postgresql.conf"
sed -i "s|^#listen_addresses =.*$|listen_addresses = '*'                    # what IP address(es) to listen on;|" "/etc/postgresql/$PG_VERSION/main/postgresql.conf"
sed -i "s|^#wal_level =.*$|wal_level = hot_standby                    # minimal, archive, or hot_standby|" "/etc/postgresql/$PG_VERSION/main/postgresql.conf"
sed -i "s|^#max_wal_senders =.*$|max_wal_senders = 3             # max number of walsender processes|" "/etc/postgresql/$PG_VERSION/main/postgresql.conf"
sed -i "s|^#checkpoint_segments =.*$|checkpoint_segments = 8                    # in logfile segments, min 1, 16MB each|" "/etc/postgresql/$PG_VERSION/main/postgresql.conf"
sed -i "s|^#wal_keep_segments  =.*$|wal_keep_segments  = 8                    # in logfile segments, 16MB each; 0 disables|" "/etc/postgresql/$PG_VERSION/main/postgresql.conf"
sed -i "s|^#hot_standby =.*$|hot_standby = on                    # on allows queries during recovery|" "/etc/postgresql/$PG_VERSION/main/postgresql.conf"

echo "Starting base backup as replicator"
sudo -u postgres pg_basebackup -h ${PRIMARY_IP} -D ${PGDATA} -U replicator -v -w -P

echo "Writing recovery.conf file"
sudo -u postgres bash -c "cat > ${PGDATA}/recovery.conf <<- _EOF1_
  standby_mode = 'on'
  primary_conninfo = 'host=${PRIMARY_IP} port=5432 user=replicator password=${POSTGRES_PASSWORD}'
  trigger_file = '/tmp/postgresql.trigger'
_EOF1_
"

echo "Startging PostgreSQL"
sudo service postgresql start

# adding file for the set of scripts for checking if cluster is configured
mkdir -p $CHECK_PATH/
chmod 700 -R  $CHECK_PATH/
echo "If this file exists, initialy a cluster was configured." > $CHECK_PATH/standby-server

# activates monitoring of postgresql instance
monit monitor postgres
