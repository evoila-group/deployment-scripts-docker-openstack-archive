#!/bin/bash

# This script only works with pre-installed and configured postgresql

export REPOSITORY_POSTGRES="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/postgres/cluster"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

# Put in here the IP-Adresses for the Primary-Instance and the Standby-Instance of the PosgresQL-HA-Configuration
export PRIMARY_IP="192.168.1.5"
export STANDBY_IP="192.168.1.6"

wget $REPOSITORY_POSTGRES/postgres-HA-template.sh --no-cache
chmod +x postgres-HA-template.sh
./postgres-HA-template.sh -u evoila -p evoila -d evoila -e openstack -i ${PRIMARY_IP} -j ${STANDBY_IP} -a primary



###################### Secondary Configuration ######################

#!/bin/bash
export REPOSITORY_POSTGRES="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/postgres/cluster"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

# Put in here the IP-Adresses for the Primary-Instance and the Standby-Instance of the PosgresQL-HA-Configuration
export PRIMARY_IP="192.168.1.5"
export STANDBY_IP="192.168.1.6"

wget $REPOSITORY_POSTGRES/postgres-HA-template.sh --no-cache
chmod +x postgres-HA-template.sh
./postgres-HA-template.sh -u evoila -p evoila -d evoila -e openstack -i ${PRIMARY_IP} -j ${STANDBY_IP} -a standby
