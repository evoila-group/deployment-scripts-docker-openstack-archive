#!/bin/bash

echo
echo "openstack_start: $OPENSTACK_START"
echo "openstack_stop: $OPENSTACK_STOP"
echo "docker_start: $DOCKER_START"
echo "docker_stop: $DOCKER_STOP"
echo

apt-cache showpkg haproxy
add-apt-repository ppa:vbernat/haproxy-1.6 -y
apt-get update
apt-get install haproxy -y

# for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
  $OPENSTACK_STOP
fi

# for docker as environment
if [ "$ENVIRONMENT" = 'docker' ]; then
  $DOCKER_STOP
fi
