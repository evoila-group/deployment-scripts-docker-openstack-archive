#!/bin/bash
echo
echo "### Starting configuration of HA-Proxy ###"
echo


# Configuration for HA-Proxy as Loadbalancer for 1 Router
if [ "$HAPROXY_ROUTER" = '1' ]; then
echo "Configuration for 1 Router"
echo "# Configuration for a HA-Proxy as Loadbalancer for 1 Router
# Port for http
${PORT_HTTP_PROXY}    ${IP_ROUTER_1}:${PORT_HTTP_ROUTER_1}" > /etc/haproxy/haproxy.conf
fi

# Aditional configuration for HA-Proxy with SSL for https
if [ "$HAPROXY_SSL" = 'ssl' ]; then
echo "Configuration for https"
echo "# Port for https
${PORT_HTTPS_PROXY}    ${IP_ROUTER_1}:${PORT_HTTPS_ROUTER_1}" >> /etc/haproxy/haproxy.conf

# Add the following line (with a value you think is reasonable) to the global section of the configuration
sed -i "/defaults/i maxconn $MAXCONN \n"  /etc/haproxy/haproxy.cfg
# Add this line, to configure the maximum size of temporary DHE keys that are generated
sed -i "/defaults/i tune.ssl.default-dh-param 2048 \n"  /etc/haproxy/haproxy.cfg

sed -i "/defaults/i option forwardfor \n"  /etc/haproxy/haproxy.cfg
sed -i "/defaults/i option http-server-close \n"  /etc/haproxy/haproxy.cfg


echo "frontend www-http
    bind ${IP_PUBLIC_HAPROXY}:${PORT_HTTP_PROXY}
    reqadd X-Forwarded-Proto:\ http
    default_backend www-backend
  frontend www-https
    bind ${IP_PUBLIC_HAPROXY}:${PORT_HTTPS_PROXY} ssl crt ${PEM_FILE_PATH}/${PEM_FILE}
    reqadd X-Forwarded-Proto:\ https
    default_backend www-backend
  backend www-backend
    redirect scheme https if !{ ssl_fc }
    server Router1 ${IP_ROUTER_1}:${PORT_HTTP_ROUTER_1} check
" >> /etc/haproxy/haproxy.cfg
fi

echo '$ModLoad imudp
$UDPServerRun 514
$UDPServerAddress 127.0.0.1' >> /etc/rsyslog.conf
service rsyslog restart

service haproxy restart
