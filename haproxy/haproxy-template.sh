#!/bin/bash

# start/stop commands for different environments
export OPENSTACK_START="service haproxy start"
export OPENSTACK_STOP="service haproxy start"
export DOCKER_START=""
export DOCKER_STOP=""

#path used to check if service is installed
export CHECK_PATH=/data/haproxy

#parameters for vhost, user and password used in the following scripts
usage() { echo "Usage: $0 [-p <string>] [-e <string>] [-l <string>] [-m <string>] [-c <string>] " 1>&2; exit 1; }

while getopts ":p:e:l:m:c:" o; do
    case "${o}" in
        p)
            MONIT_PASSWORD=${OPTARG}
            ;;
        e)
            ENVIRONMENT=${OPTARG}
            ;;
        l)
            LOG_HOST=${OPTARG}
            ;;
        m)
            LOG_PORT=${OPTARG}
            ;;
        c)
            LOG_PROTOCOL=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${MONIT_PASSWORD}" ] || [ -z "${ENVIRONMENT}" ] || [ -z "${LOG_HOST}" ] || [ -z "${LOG_PORT}" ] || [ -z "${LOG_PROTOCOL}" ]; then
    usage
fi


# export for following scripts
export ENVIRONMENT=${ENVIRONMENT}
export MONIT_PASSWORD=${MONIT_PASSWORD}
export LOG_HOST=${LOG_HOST}
export LOG_PORT=${LOG_PORT}
export LOG_PROTOCOL=${LOG_PROTOCOL}

echo "environment = ${ENVIRONMENT}"
echo "log_host=${LOG_HOST}"
echo "log_port=${LOG_PORT}"
echo "log_protocol=${LOG_PROTOCOL}"
echo "repository_haproxy = ${REPOSITORY_HAPROXY}"
echo "check_path = ${CHECK_PATH}"


# checks if service is installed
if [ -a $CHECK_PATH* ]; then
    # executes script for startup of haproxy
    chmod +x haproxy-run.sh
    ./haproxy-run.sh
else
    # loads and executes script for logging to a (central) logging instance
    wget $REPOSITORY_HAPROXY/haproxy-logging.sh --no-cache
    chmod +x haproxy-logging.sh
    ./haproxy-logging.sh

    # loads and executes script for automatic installation of haproxy
    wget $REPOSITORY_HAPROXY/haproxy-install.sh --no-cache
    chmod +x haproxy-install.sh
    ./haproxy-install.sh

    # loads includes for the monit controlfile for this database
    mkdir -p /etc/monit.d/
    wget $REPOSITORY_HAPROXY/haproxy-monitrc -P /etc/monit.d/

    # loads and executes script for configuration of haproxy
    wget $REPOSITORY_HAPROXY/haproxy-configuration.sh --no-cache
    chmod +x haproxy-configuration.sh
    ./haproxy-configuration.sh

    # loads and executes script for startup of haproxy
    wget $REPOSITORY_HAPROXY/haproxy-run.sh --no-cache
    chmod +x haproxy-run.sh
    ./haproxy-run.sh
fi

# installs monit for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
  export REPOSITORY_MONIT="${REPOSITORY_MONIT}"
  wget $REPOSITORY_MONIT/monit-template.sh --no-cache
  chmod +x monit-template.sh
  ./monit-template.sh -u monit -p ${MONIT_PASSWORD}
fi
