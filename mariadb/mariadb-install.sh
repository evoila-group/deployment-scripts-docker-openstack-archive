#!/bin/bash

echo
echo "### Starting installation of MariaDB ###"
echo

#starts with parameters for database name and root password set in mariadb-template.sh
echo
echo "mysql_database = ${MARIADB_DATABASE}"
echo "datadir = ${DATADIR}"
echo "environment = ${ENVIRONMENT}"
echo "repository_mariadb = ${REPOSITORY_MARIADB}"
echo "check_path = ${CHECK_PATH}"
echo "mariadb_major = ${MARIADB_MAJOR}"
echo "mariadb_version = ${MARIADB_VERSION}"
echo


# adds custom apparmor-profile for mariadb
echo "# vim:syntax=apparmor
# Last Modified: Thu Nov 05 12:00:00 2015
#include <tunables/global>

/usr/sbin/mysqld {
  #include <abstractions/base>
  #include <abstractions/nameservice>
  #include <abstractions/user-tmp>
  #include <abstractions/mysql>
  #include <abstractions/winbind>

  # Allow system resource access
  /sys/devices/system/cpu/ r,
  capability sys_resource,
  capability dac_override,
  capability setuid,
  capability setgid,
  capability chown,

  network tcp,

  # Allow config access
  /etc/mysql/conf.d/ r,
  /etc/mysql/conf.d/*.cnf r,
  /etc/mysql/*.cnf r,

  /etc/mysql/*.pem r,
  /etc/mysql/conf.d/* r,

  /etc/hosts.allow r,
  /etc/hosts.deny r,

  # Allow pid, socket, socket lock file access
  /var/run/mysqld/mysqld.pid rw,
  /var/run/mysqld/mysqld.sock rw,
  /var/run/mysqld/mysqld.sock.lock rw,
  /run/mysqld/mysqld.pid rw,
  /run/mysqld/mysqld.sock rw,
  /run/mysqld/mysqld.sock.lock rw,

  # Allow read/ write to /tmp
  /tmp/ r,
  /tmp/* rw,

  # Allow execution of server binary
  /usr/sbin/mysqld mr,
  /usr/sbin/mysqld-debug mr,

  # Allow plugin access
  /usr/lib/mysql/plugin/ r,
  /usr/lib/mysql/plugin/*.so* mr,

  # Allow error msg and charset access
  /usr/share/mysql/ r,
  /usr/share/mysql/** r,

  # Allow data dir access
  ${DATADIR}/ rw,
  ${DATADIR}/** rwk,

  /var/lib/mysql/ rw,
  /var/lib/mysql/** rwk,

  # Allow data files dir access
  /var/lib/mysql-files/ r,
  /var/lib/mysql-files/** rwk,
  ${DATADIR}/ r,

  # Allow log file access
  /var/log/mysql/ r,
  /var/log/mysql/** rw,
  /var/log/mysql.err rw,
  /var/log/mysql.log rw,
  /var/log/mysql/* rw,

  /sys/devices/system/node/ r,
  /sys/devices/system/node/** rwk,
  /sys/devices/system/node/node0/meminfo r,

  /proc/ r,
  /proc/** rwk,
  /proc/*/status r,

  /usr/sbin/mysqld r,
  /usr/sbin/mysqld** rwk,
}
" > /etc/apparmor.d/usr.sbin.mysqld

# reload apparmor with new custom profile for mysql
apparmor_parser -r /etc/apparmor.d/usr.sbin.mysqld
/etc/init.d/apparmor restart

ln -s /etc/apparmor.d/usr.sbin.mysqld /etc/apparmor.d/disable/
/etc/init.d/apparmor restart

# adds userdirectory for user mysql
mkdir -p /home/mysql
# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
groupadd -r mysql && useradd -r -g mysql mysql
chown -R mysql:mysql /home/mysql
chmod 760 -R /home/mysql

echo "Adding key to keyserver"
apt-get update
apt-get install software-properties-common -y
apt-get install python-software-properties -y


apt-get install software-properties-common
apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8

echo "Adding Repository to apt preferences"
add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://mirrors.n-ix.net/mariadb/repo/10.1/ubuntu xenial main'

export DEBIAN_FRONTEND=noninteractive
debconf-set-selections <<< "mariadb-server-${MARIADB_MAJOR} mysql-server/root_password password 'unused'"
debconf-set-selections <<< "mariadb-server-${MARIADB_MAJOR} mysql-server/root_password_again password 'unused'"

apt-get update && apt-get install -y -o Dpkg::Options::="--force-confold" mariadb-server

rm -rf /var/lib/apt/lists/*
rm -rf /var/lib/mysql
rm -rf ${DATADIR} && mkdir -p ${DATADIR}

# comment out a few problematic configuration values
# don't reverse lookup hostnames, they are usually another container
sed -Ei 's/^(bind-address|log)/#&/' /etc/mysql/my.cnf \
    && echo -e 'skip-host-cache\nlog-error      = /var/log/mysql/error.log' | awk '{ print } $1 == "[mysqld]" && c == 0 { c = 1; system("cat") }' /etc/mysql/my.cnf > /tmp/my.cnf \
    && mv /tmp/my.cnf /etc/mysql/my.cnf

# for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
 $OPENSTACK_STOP
fi
# for docker as environment
if [ "$ENVIRONMENT" = 'docker' ]; then
 $DOCKER_STOP
fi
