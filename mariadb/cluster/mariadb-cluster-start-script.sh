#!/bin/bash
export REPOSITORY_MARIADB_CLUSTER="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/mariadb/cluster/"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

# Put in here the IP-Adresses for the Primary-Instance and the Standby-Instance of the MariaDB-Cluster-Configuration
export PRIMARY_IP="192.168.1.205"
export NODE_IP="192.168.1.209"
export CLUSTER_IPS="192.168.1.205,192.168.1.209"

wget $REPOSITORY_MARIADB_CLUSTER/mariadb-cluster-template.sh --no-cache
chmod +x mariadb-cluster-template.sh
./mariadb-cluster-template.sh -p evoila -i ${PRIMARY_IP} -o ${PRIMARY_IP} -j ${CLUSTER_IPS} -a primary


###################### Secondary Configuration ######################

export REPOSITORY_MARIADB_CLUSTER="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/mariadb/cluster/"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

# Put in here the IP-Adresses for the Primary-Instance and the Standby-Instance of the MariaDB-Cluster-Configuration
export PRIMARY_IP="192.168.1.205"
export NODE_IP="192.168.1.209"
export CLUSTER_IPS="192.168.1.205,192.168.1.209"

wget $REPOSITORY_MARIADB_CLUSTER/mariadb-cluster-template.sh --no-cache
chmod +x mariadb-cluster-template.sh
./mariadb-cluster-template.sh -p evoila -i ${PRIMARY_IP} -o ${NODE_IP} -j ${CLUSTER_IPS} -a secondary
