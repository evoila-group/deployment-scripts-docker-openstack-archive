#!/bin/bash

export OPENSTACK_START="service mysql start"
export OPENSTACK_STOP="service mysql stop"
export CHECK_PATH=/data/mariadb-cluster-check

#parameters for dbname, user and password used in postgres-entrypoint.sh
usage() { echo "Usage: $0 [-p <string>]  [-a <string>] [-i <string>] [-o <string>] [-j <string>]" 1>&2; exit 1; }

while getopts ":p:a:i:o:j:" o; do
    case "${o}" in
        p)
            MARIADB_PASSWORD=${OPTARG}
            ;;
        a)
            HA_INSTANCE_TYPE=${OPTARG}
            ;;
        i)
            PRIMARY_IP=${OPTARG}
            ;;
        o)
            NODE_IP=${OPTARG}
            ;;
        j)
            CLUSTER_IPs=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${MARIADB_PASSWORD}" ] || [ -z "${HA_INSTANCE_TYPE}" ] || [ -z "${PRIMARY_IP}" ] || [ -z "${NODE_IP}" ]  || [ -z "${CLUSTER_IPs}" ]; then
    usage
fi

#exports from parameters for following scripts
export HA_INSTANCE_TYPE=${HA_INSTANCE_TYPE}
export PRIMARY_IP=${PRIMARY_IP}
export CLUSTER_IPs=${CLUSTER_IPs}
export NODE_IP=${NODE_IP}
export MARIADB_PASSWORD=${MARIADB_PASSWORD}

echo "check_path = ${CECK_PATH}"
echo "ha_instance_type = ${HA_INSTANCE_TYPE}"
echo "primary_ip = ${PRIMARY_IP}"
echo "cluster_ips = ${CLUSTER_IPs}"
echo "node_ip = ${NODE_IP}"
echo "mariabd_password = ${MARIADB_PASSWORD}"
echo "repository_mariadb = ${REPOSITORY_MARIADB_CLUSTER}"


# checks if service is installed
if [ -a $CHECK_PATH* ]; then
  wget $REPOSITORY_MARIADB_CLUSTER/mariadb-cluster-run.sh
  chmod +x mariadb-cluster-run.sh
  ./mariadb-cluster-run.sh

else
  if [ "$HA_INSTANCE_TYPE" = 'primary' ]; then
    # loads and executes script for configuration of the primary-mysql-instance for HA
    echo "Running configuration for primary node"
    wget $REPOSITORY_MARIADB_CLUSTER/mariadb-cluster-primary-conf.sh
    chmod +x mariadb-cluster-primary-conf.sh
    ./mariadb-cluster-primary-conf.sh
  fi

  if [ "$HA_INSTANCE_TYPE" = 'secondary' ]; then
    # loads and executes script for configuration of the secondary-mysql-instance for HA
    echo "Running configuration for secondary node"
    wget $REPOSITORY_MARIADB_CLUSTER/mariadb-cluster-secondary-conf.sh
    chmod +x mariadb-cluster-secondary-conf.sh
    ./mariadb-cluster-secondary-conf.sh
  fi
fi
