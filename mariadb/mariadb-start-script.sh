#!/bin/bash

export REPOSITORY_MARIADB="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/mariadb"
export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/monit"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

wget $REPOSITORY_MARIADB/mariadb-template.sh --no-cache
chmod +x mariadb-template.sh
./mariadb-template.sh -d evoila -p evoila -e openstack -l 172.24.102.12 -m 5002
