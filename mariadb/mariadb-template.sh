#!/bin/bash
export MARIADB_MAJOR=10.1
export MARIADB_VERSION=10.1.12
export DATADIR=/data/mysql

#path used to check if service is installed
export CHECK_PATH=$DATADIR

# start/stop commands for different environments
export OPENSTACK_START="service mysql start"
export OPENSTACK_STOP="service mysql stop"
export DOCKER_START="mysqld"
export DOCKER_STOP="mysqld stop"

#configuration for logging
export LOG_HOST="172.24.102.12"
export LOG_PORT="5002"
export LOG_SENDING_PROTOCOL="tcp"


#parameters for dbname, user and password and environment used in mariadb-configuration.sh and mariadb-run.sh
usage() { echo "Usage: $0 [-d <string>] [-p <string>] [-e <string>] [-l <string>] [-m <string>]" 1>&2; exit 1; }

while getopts ":d:p:e:l:m:" o; do
    case "${o}" in
        d)
            MARIADB_DATABASE=${OPTARG}
            ;;
        p)
            MARIADB_ROOT_PASSWORD=${OPTARG}
            ;;
        e)
            ENVIRONMENT=${OPTARG}
            ;;
        l)
            LOG_HOST=${OPTARG}
            ;;
        m)
            LOG_PORT=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

shift $((OPTIND-1))

if [ -z "${MARIADB_DATABASE}" ] || [ -z "${MARIADB_ROOT_PASSWORD}" ] || [ -z "${ENVIRONMENT}" ] || [ -z "${LOG_HOST}" ] || [ -z "${LOG_PORT}" ]; then
    usage
fi


# export for following scripts
export MARIADB_DATABASE=${MARIADB_DATABASE}
export MARIADB_ROOT_PASSWORD=${MARIADB_ROOT_PASSWORD}
export ENVIRONMENT=${ENVIRONMENT}
export LOG_HOST=${LOG_HOST}
export LOG_PORT=${LOG_PORT}

echo "mysql_database = ${MARIADB_DATABASE}"
echo "datadir = ${DATADIR}"
echo "environment = ${ENVIRONMENT}"
echo "repository_mariadb = ${REPOSITORY_MARIADB}"
echo "mariadb_major = ${MARIADB_MAJOR}"
echo "mariadb_version = ${MARIADB_VERSION}"
echo "check_path = ${CHECK_PATH}"
echo "log_host=${LOG_HOST}"
echo "log_port=${LOG_PORT}"


# checks if service is installed
if [ -a $CHECK_PATH* ]; then
    # executes script for startup of mariadb
    chmod +x mariadb-run.sh
    ./mariadb-run.sh mysql
else
    if [ "$ENVIRONMENT" = 'openstack' ]; then
      # loads and executes script for logging to a (central) logging instance
      wget $REPOSITORY_MARIADB/mariadb-logging.sh
      chmod +x mariadb-logging.sh
      ./mariadb-logging.sh
    fi
    # loads and executes script for automatic installation of mariadb
    wget $REPOSITORY_MARIADB/mariadb-install.sh --no-cache
    chmod +x mariadb-install.sh
    ./mariadb-install.sh

    # loads includes for the monit controlfile for this database
    mkdir -p /etc/monit.d/
    wget $REPOSITORY_MARIADB/mariadb-monitrc -P /etc/monit.d/

    # loads and executes script for configuration of mariadb
    wget $REPOSITORY_MARIADB/mariadb-configuration.sh --no-cache
    chmod +x mariadb-configuration.sh
    ./mariadb-configuration.sh

    # loads and executes script for startup of mariadb
    wget $REPOSITORY_MARIADB/mariadb-run.sh --no-cache
    chmod +x mariadb-run.sh
    ./mariadb-run.sh mysql
fi

# installs monit for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
  export REPOSITORY_MONIT="${REPOSITORY_MONIT}"
  wget $REPOSITORY_MONIT/monit-template.sh
  chmod +x monit-template.sh
  ./monit-template.sh -u monit -p ${MARIADB_ROOT_PASSWORD}
fi


$REPOSITORY_MARIADB