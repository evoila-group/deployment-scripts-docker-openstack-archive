#!/bin/bash

export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/monit"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

wget $REPOSITORY_MONIT/monit-template.sh --no-cache
chmod +x monit-template.sh
./monit-template.sh -u evoila -p evoila
