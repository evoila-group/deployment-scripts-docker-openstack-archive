#!/bin/bash

#path used to check if service is installed
export CHECK_PATH=/etc/monit/

# start/stop commands for different environments
export OPENSTACK_START="monit reload"
export OPENSTACK_STOP="service monit stop"


#starts with parameters for user and password
usage() { echo "Usage: $0 [-u <string>] [-p <string>]" 1>&2; exit 1; }

while getopts ":u:p:" o; do
    case "${o}" in
        p)
            MONIT_PASSWORD=${OPTARG}
            ;;
        u)
            MONIT_USER=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${MONIT_USER}" ] || [ -z "${MONIT_PASSWORD}" ]; then
    usage
fi

echo "monit_user = ${MONIT_USER}"
echo "monit_password = ${MONIT_PASSWORD}"

# export for following scripts
export MONIT_USER=${MONIT_USER}
export MONIT_PASSWORD=${MONIT_PASSWORD}

echo "monit_user = ${MONIT_USER}"
echo "repository_monit = ${REPOSITORY_MONIT}"
echo "check_path = ${CHECK_PATH}"

# checks if service is installed
if [ -a $CHECK_PATH* ]; then
    # executes script for startup of monit
    chmod +x monit-run.sh
    ./monit-run.sh monit
else
    if [ "$ENVIRONMENT" = 'openstack' ]; then
      # loads and executes script for automatic installation of monit
      wget $REPOSITORY_MONIT/monit-install.sh
      chmod +x monit-install.sh
      ./monit-install.sh
    fi
    # loads and executes script for configuration of monit
    wget $REPOSITORY_MONIT/monit-configuration.sh
    chmod +x monit-configuration.sh
    ./monit-configuration.sh

    # loads and executes script for startup of monit
    wget $REPOSITORY_MONIT/monit-run.sh
    chmod +x monit-run.sh
    ./monit-run.sh monit
fi
