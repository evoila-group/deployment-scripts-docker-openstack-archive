#!/bin/bash
export HOME=/root

#path used to check if service is installed
export CHECK_PATH=/data/

#configuration for logging
export LOG_HOST="172.24.102.12"
export LOG_PORT="5002"
export LOG_SENDING_PROTOCOL="tcp"

# export for following scripts
echo "repository_emqtt = ${REPOSITORY_EMQTT}"
echo "check_path = ${CHECK_PATH}"

# checks if service is installed
if [ -a $CHECK_PATH* ]; then
    # executes script for startup of emqtt
    chmod +x emqtt-run.sh
    ./emqtt-run.sh emqtt
else
    # loads and executes script for logging to a (central) logging instance
    wget $REPOSITORY_EMQTT/emqtt-logging.sh
    chmod +x emqtt-logging.sh
    ./emqtt-logging.sh

    # loads and executes script for automatic installation of emqtt
    wget https://github.com/emqtt/emqttd/releases/tag/0.16.0

    wget $REPOSITORY_EMQTT/emqtt-install.sh
    chmod +x emqtt-install.sh
    ./emqtt-install.sh

    # loads and executes script for configuration of emqtt
    wget $REPOSITORY_EMQTT/emqtt-configuration.sh
    chmod +x emqtt-configuration.sh
    ./emqtt-configuration.sh

    # loads and executes script for startup of emqtt
    wget $REPOSITORY_EMQTT/emqtt-run.sh
    chmod +x emqtt-run.sh
    ./emqtt-run.sh emqttd
fi
