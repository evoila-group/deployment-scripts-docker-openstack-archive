#!/bin/bash

export REPOSITORY_EMQTT="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/emqtt"
export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/monit"

wget $REPOSITORY_EMQTT/emqtt-template.sh
chmod +x emqtt-template.sh
./emqtt-template.sh
