

mkdir /data/mongodb/configdb


mongod --configsvr --dbpath /data/mongodb/configdb --port 27019 &


mongos --configdb 192.168.1.203:27019,192.168.1.204:27019,192.168.1.205:27019 &





# enables using key for autorization
echo "
storage:
  dbPath: \"${MONGO_DATA_PATH}\"
  directoryPerDB: true
  journal:
    enabled: true
sharding:
   configDB: \"data\"mongodb\"configdb\"
systemLog:
   destination: file
   path: "/var/log/mongodb/mongod.log"
   logAppend: true
security:
   authorization: "enabled"
   keyFile: /data/mongodb-key/key-$REPSET_NAME
net:
   port: 27019" > $MONGOS_CONF
