#!/bin/bash

# This script only works with pre-installed and configured mongodb

export REPOSITORY_MONGO_HA="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/mongodb/cluster"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

export REPSET_NAME="shared_cluster_1"
# Hier den zu verwendenden Key-Namen für die Authentifikation im MongoDB-Cluster angeben.
#Der Key muss vor der Installation im Verzeichnis /data/mongodb-key abgelegt werden.
#Der Key-Namen sollte den Namen des Replica-Sets bzw. Clusters enthalten ( => z.B.: key-mongo_cluster_1 )
export MONGODB_KEY="key-$REPSET_NAME"


wget $REPOSITORY_MONGO_HA/mongo-HA-repset-template.sh --no-cache
chmod +x mongo-HA-repset-template.sh
./mongo-HA-repset-template.sh -d evoila -u evoila -p evoila -r $REPSET_NAME -k $MONGODB_KEY -s $MONGODB_KEY_SOURCE -m 192.168.1.3 -s "192.168.1.4,192.168.1.5" -a primary



###################### Secondary Configuration ######################



export REPOSITORY_MONGO_HA="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/mongodb/cluster"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

export REPSET_NAME="shared_cluster_1"
# Hier den zu verwendenden Key-Namen für die Authentifikation im MongoDB-Cluster angeben.
#Der Key muss vor der Installation im Verzeichnis /data/mongodb-key abgelegt werden.
#Der Key-Namen sollte den Namen des Replica-Sets bzw. Clusters enthalten ( => z.B.: key-mongo_cluster_1 )
export MONGODB_KEY="key-$REPSET_NAME"


wget $REPOSITORY_MONGO_HA/mongo-HA-repset-template.sh --no-cache
chmod +x mongo-HA-repset-template.sh
./mongo-HA-repset-template.sh -d evoila -u evoila -p evoila -r $REPSET_NAME -k $MONGODB_KEY -s $MONGODB_KEY_SOURCE -a secondary
