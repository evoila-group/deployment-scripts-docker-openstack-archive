#!/bin/bash

if [ "$HA_INSTANCE_TYPE" = 'primary' ]; then
echo "### Running configuration of MongoDB Replicaset Primary Instance ... "
fi

if [ "$HA_INSTANCE_TYPE" = 'secondary' ]; then
echo "### Running configuration of MongoDB Replicaset Secondary Instance ... "
fi


monit unmonitor mongodb
monit summary
# check connection to other replicaset members


#echo "$MONGODB_KEY" >> /data/mongodb-key/$MONGODB_KEY
chown mongodb:mongodb /data/mongodb-key/$MONGODB_KEY
chmod 700 /data/mongodb-key/
chmod 700 /data/mongodb-key/$MONGODB_KEY

# adds clusterAdmin rights to mongodb-user
echo "Adding clusterAdmin rights to mongodb-user"

echo "use admin
db.auth(\"${MONGO_USER_ADMIN}\",\"${MONGO_PASSWORD}\")
db.grantRolesToUser(
    \"${MONGO_USER_ADMIN}\",
    [
      { role: \"root\", db: \"admin\" },
      { role: \"clusterAdmin\", db: \"admin\" }
    ]
)" > grant-roles.js
mongo < grant-roles.js

echo "Output of insert for mongo shell for debuging:"
echo "use admin
db.auth(\"${MONGO_USER_ADMIN}\",\"${MONGO_PASSWORD}\")
db.grantRolesToUser(
    \"${MONGO_USER_ADMIN}\",
    [
      { role: \"root\", db: \"admin\" },
      { role: \"clusterAdmin\", db: \"admin\" }
    ]
)"


service mongod stop
echo "Waiting for the mongo server to stop for 10 seconds..."
sleep 10



# enables using key for autorization
echo "
storage:
  dbPath: \"${MONGO_DATA_PATH}\"
  directoryPerDB: true
  journal:
    enabled: true
systemLog:
   destination: file
   path: "/var/log/mongodb/mongod.log"
   logAppend: true
security:
   authorization: "enabled"
   keyFile: /data/mongodb-key/$MONGODB_KEY
net:
   port: 27017" > $MONGO_CONF

# additional config for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
   echo "processManagement:
       pidFilePath: "/var/run/mongodb/mongod.pid"" >> $MONGO_CONF
fi



# only for secondary
if [ "$HA_INSTANCE_TYPE" = 'secondary' ]; then
  rm -R /data/mongodb/*
  mkdir -p /data/mongodb/db
  chown -R mongodb:mongodb /data/mongodb/db
fi



# specifies the replica set name through the --replSet command-line option
mongod --replSet "${REPSET_NAME}" --config /etc/mongod.conf &
echo "Waiting for restart of mongo server with new configuration for 15 seconds..."
sleep 15



# only for primary
if [ "$HA_INSTANCE_TYPE" = 'primary' ]; then

  # Initiate the replica set on the replica set member
  echo "use admin
  db.auth(\"${MONGO_USER_ADMIN}\",\"${MONGO_PASSWORD}\")
  config = {
      _id : \"${REPSET_NAME}\",
       members : [
        {_id : 0, host : \"${INSTANCE_IP_PRIMARY}\"}," > add-config-repset.js;

  IFS=","
  secIpsArray=($INSTANCE_IPS_SECONDARYS)
for ((i=0; i<${#secIpsArray[@]}; i++)); do
  let "id = $i + 1"
  echo  "{_id : $i+1, host : \"${secIpsArray[$i]}\"}," >> add-config-repset.js
done

echo "
       ]
  };
  rs.initiate(config)
  " >> add-config-repset.js
  #configs repset in mongo shell
  mongo < add-config-repset.js

  # check the status of the replica set
  echo "use admin
  db.auth(\"${MONGO_USER_ADMIN}\",\"${MONGO_PASSWORD}\")
  rs.status()
  " > rs-status.js
  #creates userAdmin for mongod
  mongo < rs-status.js
fi

mkdir -p $CHECK_PATH
echo "This instance is configures for a mongodb-cluster. This file and the folder Path/to/file/mongodb-cluster-check is only used for control of the configuration state in the deployment scripts." > $CHECK_PATH/check-file

# activates monitoring of mongodb-HA replicaset-instance
# monit monitor mongodb
