#!/bin/bash

export MONGO_DATA_PATH=/data/mongodb/db
export MONGO_CONF=/etc/mongod.conf
export MONGOS_CONF=/etc/mongos.conf

#path used to check if service is installed
export CHECK_PATH=/data/mongodb-cluster-check

#parameters for dbname, user and password used in following scripts for mongo
usage() {
    echo "Error: $1"
    echo "Usage for primary instance: $0 [-d <string>] [-p <string>] [-u <string>] [-r <string>] [-k <string> optional] [-q <string> optional] [-t <string> optional] [-m <string>] [-s <string>] [-a <string>]"
    echo "Usage for secondary instance: $0 [-d <string>] [-p <string>] [-u <string>] [-r <string>] [-k <string> optional] [-q <string> optional] [-t <string> optional] [-a <string>]"
    1>&2;  exit 1;
}

while getopts ":d:p:u:r:m:s:a:k:q:t" o; do
    case "${o}" in
        d)
            MONGO_DB=${OPTARG}
            ;;
        p)
            MONGO_PASSWORD=${OPTARG}
            ;;
        u)
            MONGO_USER_ADMIN=${OPTARG}
            ;;
        r)
            REPSET_NAME=${OPTARG}
            ;;
        k)
            MONGODB_KEY=${OPTARG}
            ;;
        q)
            MONGODB_KEY_SOURCE=${OPTARG}
            ;;
        t)
            MONGODB_KEY_PARAMETER=${OPTARG}
            ;;
        m)
            INSTANCE_IP_PRIMARY=${OPTARG}
            ;;
        s)
            INSTANCE_IPS_SECONDARYS=${OPTARG}
            ;;
        a)
            HA_INSTANCE_TYPE=${OPTARG}
            ;;
        *)
            usage "unknown parameter"
            ;;
    esac
done
shift $((OPTIND-1))

if [ "$HA_INSTANCE_TYPE" = 'primary' ]; then
  if [ -z "${MONGO_DB}" ] || [ -z "${MONGO_USER_ADMIN}" ] || [ -z "${MONGO_PASSWORD}" ] || [ -z "${REPSET_NAME}" ] || [ -z "${INSTANCE_IP_PRIMARY}" ] || [ -z "${INSTANCE_IPS_SECONDARYS}" ] || [ -z "${HA_INSTANCE_TYPE}" ] ; then
      usage "parameters for primary not correct"
  fi
fi

if [ "$HA_INSTANCE_TYPE" = 'secondary' ]; then
  if [ -z "${MONGO_DB}" ] || [ -z "${MONGO_USER_ADMIN}" ] || [ -z "${MONGO_PASSWORD}" ] || [ -z "${REPSET_NAME}" ] || [ -z "${HA_INSTANCE_TYPE}" ] ; then
      usage "parameters for secondary not correct"
  fi
fi


# export for following scripts
export MONGO_DB=${MONGO_DB}
export MONGO_USER_ADMIN=${MONGO_USER_ADMIN}
export MONGO_PASSWORD=${MONGO_PASSWORD}
export ENVIRONMENT=${ENVIRONMENT}

export REPSET_NAME=${REPSET_NAME}
export HA_INSTANCE_TYPE=${HA_INSTANCE_TYPE}
export MONGODB_KEY=${MONGODB_KEY}
export INSTANCE_IP_PRIMARY=${INSTANCE_IP_PRIMARY}
export INSTANCE_IPS_SECONDARYS=${INSTANCE_IPS_SECONDARYS}

echo "mongo_db = ${MONGO_DB}"
echo "mongo_user = ${MONGO_USER_ADMIN}"
echo "repository_mongo_HA = ${REPOSITORY_MONGO_HA}"
echo "check_path = ${CHECK_PATH}"
echo
echo "ha_instance_type = ${HA_INSTANCE_TYPE}"
echo "repset_name = ${REPSET_NAME}"
if [ "$HA_INSTANCE_TYPE" = 'primary' ]; then
  echo "instance_ip_primary = ${INSTANCE_IP_PRIMARY}"
  echo "instance_ips_secondarys = ${INSTANCE_IPS_SECONDARYS}"
fi



# checks if service is installed
if [ -a $CHECK_PATH* ]; then
    # executes script for startup of mongodb
    chmod +x mongo-HA-run.sh
    ./mongo-HA-run.sh
else
    mkdir -p /data/mongodb-key
    if [  -z $MONGODB_KEY_SOURCE ]; then
      # writes Key given as Parameter in file
      echo "$MONGODB_KEY_PARAMETER" >> /data/mongodb-key/$MONGODB_KEY
      echo
      echo "Key for Replicaset or Cluster writen in file from parameter.
      (If you did not put an key in directly in the parameters or provid a key another way this will result in an error.)"
      echo
    else
      # downloads Key from a given source
      wget $MONGODB_KEY_SOURCE/$MONGODB_KEY -P /data/mongodb-key/$MONGODB_KEY
      echo
      echo "Key for Replicaset or Cluster downloaded from source.
      (If you did not declare a source for the key in the parameters or provid a key another way this will result in an error.)"
      echo
    fi

    # loads and executes script for configuration of mongodb replication set
    wget $REPOSITORY_MONGO_HA/mongo-HA-repset-conf.sh
    chmod +x mongo-HA-repset-conf.sh
    ./mongo-HA-repset-conf.sh
fi

# activates monitoring of mongodb-HA replicaset-instance
monit monitor mongodb
