#!/bin/bash

export REPOSITORY_MONGO="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/mongodb"
export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD/monit"
export REPOSITORY_MAIN="https://bitbucket.org/meshstack/deployment-scripts-docker-openstack/raw/HEAD"

wget $REPOSITORY_MONGO/mongo-template.sh --no-cache
chmod +x mongo-template.sh
./mongo-template.sh -d evoila -u evoila -v evoila -p evoila -e openstack
