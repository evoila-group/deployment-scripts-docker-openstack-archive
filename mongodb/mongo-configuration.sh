#!/bin/bash
echo
echo "### Starting configuration of MongoDB ... ###"
echo

export LC_ALL=C

#starts with parameters for dbname, user and password set in mongo-template.sh
echo "mongo_db = ${MONGO_DB}"
echo "mongo_user = ${MONGO_USER_ADMIN}"
echo "environment = ${ENVIRONMENT}"


#creates config-file

#folder for config-file
mkdir -p /etc/mongodb/
#folder for pid-file
mkdir -p /var/run/mongodb/
chown -R mongodb:mongodb /var/run/mongodb/

echo "
storage:
  dbPath: \"${MONGO_DATA_PATH}\"
  directoryPerDB: true
  journal:
    enabled: true
systemLog:
   destination: file
   path: "/var/log/mongodb/mongod.log"
   logAppend: true
security:
   authorization: "disabled"
net:
   port: 27017" > $MONGO_CONF

# for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
 echo "processManagement:
     pidFilePath: "/var/run/mongodb/mongod.pid"" >> $MONGO_CONF
 $OPENSTACK_START
fi
# for docker as environment
if [ "$ENVIRONMENT" = 'docker' ]; then
 $DOCKER_START &
fi

echo "Waiting for the mongo server to start for 90 seconds..."

sleep 60


echo "Executing default user setup"

echo "use admin
db.createUser(
  {
    user: \"$MONGO_USER_ADMIN\",
    pwd: \"$MONGO_PASSWORD\",
    roles: [ 
      { role: \"userAdminAnyDatabase\", db: \"admin\" }, 
      { role: \"dbAdminAnyDatabase\", db: \"admin\" }, 
      { role: \"readWriteAnyDatabase\", db: \"admin\" }
    ]
  }
)" > install-admin.js

#creates user for Service Broker
mongo < install-admin.js

rm install-admin.js

echo "use servicedb
db.createUser(
  {
    user: \"$MONGO_USER_SERVICEBROKER\",
    pwd: \"$MONGO_PASSWORD\",
    roles: [ { role: \"readWrite\", db: \"servicedb\" } ]
  }
)" > install-sb-user.js

#creates userAdmin for mongod
mongo < install-sb-user.js

rm install-sb-user.js

# for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
 $OPENSTACK_STOP
fi
# for docker as environment
if [ "$ENVIRONMENT" = 'docker' ]; then
 $DOCKER_STOP &
fi

echo "Waiting for the mongo server to stop for 15 seconds..."

sleep 15


echo "
storage:
  dbPath: \"${MONGO_DATA_PATH}\"
  directoryPerDB: true
  journal:
    enabled: true
systemLog:
   destination: file
   path: \"/var/log/mongodb/mongod.log\"
   logAppend: true
security:
   authorization: \"enabled\"
net:
   port: 27017" > $MONGO_CONF

# additional config for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
  echo "processManagement:
      pidFilePath: \"/var/run/mongodb/mongod.pid\"" >> $MONGO_CONF
fi
