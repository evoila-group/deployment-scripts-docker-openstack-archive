#!/bin/bash

PASSWORD= $1

# System auf aktuellen stand bringen „Patchen“
apt-get update
apt-get upgrade -y

# icinga2 repo-setup
add-apt-repository ppa:formorer/icinga -y
apt-get update

# Installation icinga2
apt-get install icinga2 -y
apt-get install nagios-plugins* -y

/etc/init.d/icinga2 start



# installiert eine mysql datenbank
echo "mysql-server-5.5 mysql-server/root_password password "$PASSWORD | debconf-set-selections
echo "mysql-server-5.5 mysql-server/root_password_again password "$PASSWORD | debconf-set-selections
apt-get install mysql-server mysql-client -y


echo "icinga2-ido-mysql icinga2-ido-mysql/app-password-confirm password "$PASSWORD | debconf-set-selections
echo "icinga2-ido-mysql icinga2-ido-mysql/password-confirm password "$PASSWORD | debconf-set-selections
echo "icinga2-ido-mysql icinga2-ido-mysql/mysql/admin-pass password "$PASSWORD | debconf-set-selections
echo "icinga2-ido-mysql icinga2-ido-mysql/mysql/app-pass password "$PASSWORD | debconf-set-selections
echo "icinga2-ido-mysql icinga2-ido-mysql/db/app-user string icinga2" | debconf-set-selections
echo "icinga2-ido-mysql icinga2-ido-mysql/purge boolean false" | debconf-set-selections
echo "icinga2-ido-mysql icinga2-ido-mysql/internal/reconfiguring boolean false" | debconf-set-selections
echo "icinga2-ido-mysql icinga2-ido-mysql/dbconfig-install boolean true" | debconf-set-selections
echo "icinga2-ido-mysql icinga2-ido-mysql/database-type string mysql" | debconf-set-selections
echo "icinga2-ido-mysql icinga2-ido-mysql/mysql/admin-user string root" | debconf-set-selections
echo "icinga2-ido-mysql icinga2-ido-mysql/db/dbname string icinga2" | debconf-set-selections
echo "icinga2-ido-mysql icinga2-ido-mysql/enable boolean false" | debconf-set-selections
apt-get install icinga2-ido-mysql -y


# datenbank & benutzer erstellen
mysql --user=root --password=waldemar --execute="GRANT SELECT, INSERT, UPDATE, DELETE, DROP, CREATE VIEW, INDEX, EXECUTE ON icinga2.* TO 'icinga'@'localhost' IDENTIFIED BY 'icinga';CREATE DATABASE icingaweb_db;GRANT SELECT, INSERT, UPDATE, DELETE, DROP, CREATE VIEW, INDEX, EXECUTE ON icingaweb_db.* TO 'icinga'@'localhost' IDENTIFIED BY 'icinga';"
icinga2 feature enable ido-mysql
service icinga2 restart

# Installation apache2 
apt-get install apache2 -y
icinga2 feature enable command
service icinga2 restart

# icingaweb2 repo-setup
wget -O - http://packages.icinga.org/icinga.key | apt-key add -
add-apt-repository 'deb http://packages.icinga.org/ubuntu icinga-trusty main'
apt-get update

# Installation icingaweb2 
apt-get install icingaweb2 -y

# token für icingaweb2
TOKEN=$( icingacli setup token create )

# zusätzliche anpassungen für icingaweb2
sed -i 's/;date.timezone =/date.timezone = "Europe\/Berlin"/g' /etc/php5/apache2/php.ini
apt-get install php5-intl php5-gd php5-imagick php5-pgsql -y
service apache2 restart

echo "TOKEN IS: $TOKEN"
